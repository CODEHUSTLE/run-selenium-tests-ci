FROM maven:3.5.4-jdk-8-alpine as maven

COPY ./pom.xml ./pom.xml

COPY ./src ./src

RUN mvn -Dmaven.repo.local=.m2/repository clean package

RUN mvn compile

FROM openjdk:8u171-jre-alpine

WORKDIR /seleniumci

COPY --from=maven target/SimpleTest-*.jar ./seleniumci/SimpleTest.jar

CMD ["java", "-jar", "./seleniumci/SimpleTest.jar"]
